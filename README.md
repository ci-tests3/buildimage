# Official Gothic 2 Online build image
Build image created for cross-compilation of Gothic 2 Online. Includes CMake v3.22 and GCC for i686, arm32v7 and aarch64.

## Compilation for x86
To compile binary for 32-bit, use this compilers:
```bash
i686-linux-gnu-gcc
i686-linux-gnu-g++
```

## Compilation for ARM
To compile binary for 32-bit arm, use this compilers:
```bash
arm-linux-gnueabi-gcc
arm-linux-gnueabi-g++
```

To compile binary for 64-bit arm, use this compilers:
```bash
aarch64-linux-gnu-gcc
aarch64-linux-gnu-g++
```
