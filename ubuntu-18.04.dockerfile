ARG ARCHITECTURE
FROM ${ARCHITECTURE}/ubuntu:18.04

# fix for error under arm-64 related to libc-bin
RUN rm /var/lib/dpkg/info/libc-bin.*

# update and install required packages
RUN apt-get clean
RUN apt-get update -q && apt install -y -q libc-bin build-essential libssl-dev curl git

# install cmake from source
COPY install-cmake.sh .
RUN chmod +x install-cmake.sh && ./install-cmake.sh
RUN rm install-cmake.sh

# debug prints
RUN ldd --version
RUN cmake --version